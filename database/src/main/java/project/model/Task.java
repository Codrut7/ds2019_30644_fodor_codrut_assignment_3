package project.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
public class Task implements Serializable {

    /** ID of the task. */
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long taskId;

    private LocalDateTime startDate;

    private LocalDateTime endDate;

    private String activity;

    @ManyToOne
    @JoinColumn(name="patientId", nullable=true)
    @JsonIgnore
    private Patient patient;

    public Task(){

    }

    public Task(LocalDateTime startDate, LocalDateTime endDate, String activity, Patient patient) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.activity = activity;
        this.patient = patient;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
}
