package project.model;

import javax.persistence.*;

/**
 * Class used to represent the doctor entity in the project.
 *
 * @author Codrut
 */
@Entity
public class Doctor {

    /** ID of the doctor. */
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long doctorId;

    /** Name of the doctor. */
    private String name;

    /** Hospital of the doctor. */
    private String hosptial;

    @OneToOne
    @JoinColumn(name="id")
    private User user;

    public Doctor(String name, String hosptial, User user) {
        this.name = name;
        this.hosptial = hosptial;
        this.user = user;
    }

    public Doctor(){

    }


    public Long getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Long doctorId) {
        this.doctorId = doctorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHosptial() {
        return hosptial;
    }

    public void setHosptial(String hosptial) {
        this.hosptial = hosptial;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
