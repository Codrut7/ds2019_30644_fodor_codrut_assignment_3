package project.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

/**
 * Class used to represent the medication entity in the project.
 *
 * @author Codrut
 */
@Entity
public class Medication {

    /** ID of the medication. */
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long medicationId;

    /** Name of the medication. */
    private String name;

    /** List of side effects of the medication. */
    @ElementCollection(targetClass=String.class)
    private List<String> sideEffects;

    /** Dosage of the medication. (IN MG)*/
    private double dosage;

    @ManyToMany
    @JoinTable(
            name = "medications_plans",
            joinColumns = @JoinColumn(name = "medicationPlanId"),
            inverseJoinColumns = @JoinColumn(name = "medicationId"))
    @JsonIgnore
    private Set<MedicationPlan> medicationPlans;

    public Medication(){

    }

    public Medication(String name, List<String> sideEffects, double dosage) {
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
    }


    public Long getMedicationId() {
        return medicationId;
    }

    public void setMedicationId(Long medicationId) {
        this.medicationId = medicationId;
    }

    public Set<MedicationPlan> getMedicationPlans() {
        return medicationPlans;
    }

    public void setMedicationPlans(Set<MedicationPlan> medicationPlans) {
        this.medicationPlans = medicationPlans;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(List<String> sideEffects) {
        this.sideEffects = sideEffects;
    }

    public double getDosage() {
        return dosage;
    }

    public void setDosage(double dosage) {
        this.dosage = dosage;
    }
}
