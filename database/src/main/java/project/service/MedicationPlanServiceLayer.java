package project.service;

import org.springframework.beans.factory.annotation.Autowired;
import project.dao.MedicationPlanRepository;
import project.model.Medication;
import project.model.MedicationPlan;

import java.util.ArrayList;
import java.util.List;

public class MedicationPlanServiceLayer {

    @Autowired
    private MedicationPlanRepository medicationPlanRepository;

    public void createMedicationPlan(MedicationPlan medicationPlan){
        medicationPlanRepository.save(medicationPlan);
    }
}
