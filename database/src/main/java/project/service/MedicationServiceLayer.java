package project.service;


import org.springframework.beans.factory.annotation.Autowired;
import project.dao.MedicationRepository;
import project.model.Medication;

import java.util.ArrayList;
import java.util.List;

public class MedicationServiceLayer {

    @Autowired
    private MedicationRepository medicationRepository;


    public List<Medication> getAllMedications() {
        List<Medication> results = new ArrayList<>();

        for (Medication medication : medicationRepository.findAll()) {
            results.add(medication);
        }

        return results;
    }

    public void deleteMedication(Medication medication){
        medicationRepository.delete(medication);
    }

    public Medication findByName(String name){
        return medicationRepository.findByName(name);
    }

    public void createMedication(Medication m) {
        medicationRepository.save(m);
    }
}
