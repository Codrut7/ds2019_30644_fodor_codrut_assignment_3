package project.service;

import io.grpc.stub.StreamObserver;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
import project.Service;
import project.model.Medication;
import project.model.Patient;
import project.model.User;
import project.sendMedicationPlanServiceGrpc;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@org.springframework.stereotype.Service
public class RpcService extends sendMedicationPlanServiceGrpc.sendMedicationPlanServiceImplBase{

    @Autowired
    private PatientServiceLayer patientServiceLayer;
    @Autowired
    private UserServiceLayer userServiceLayer;

    @Override
    public void send(Service.userName request, StreamObserver<Service.MedicationPlan> responseObserver) {
        // HelloRequest has toString auto-generated.
        String name = request.getName();
        User user = userServiceLayer.findByUsername(name);
        Patient patient = patientServiceLayer.findPatientByUser(user);

        List<Service.Medication> medications = new ArrayList<>();
        for(Medication medication : patient.getMedicationPlan().getMedications()){

           Service.Medication med = Service.Medication.newBuilder()
           .setId(medication.getMedicationId())
           .setName(medication.getName())
           .setDosage(medication.getDosage())
           .build();

           medications.add(med);
        }

        // Daca date-urile de start si end pentru medicament is null ( ceea ce sunt mereu ) am adaugat + 6 ore la timpul curent . ( sa isi ia in 6 ore medicamentu )

        Service.MedicationPlan response = Service.MedicationPlan.newBuilder()
                .setStartDate( patient.getMedicationPlan().getStartDate() != null ? patient.getMedicationPlan().getStartDate().getTime() : System.currentTimeMillis())
                .setEndDate(patient.getMedicationPlan().getEndDate() != null ? patient.getMedicationPlan().getStartDate().getTime() : DateUtils.addHours(new Date(System.currentTimeMillis()), 6).getTime())
                .addAllMedications(medications)
                .setIntakeNumber(patient.getMedicationPlan().getIntakeNumber())

            .build();
        // Use responseObserver to send a single response back
        responseObserver.onNext(response);

        // When you are done, you must call onCompleted.
        responseObserver.onCompleted();
    }
}
