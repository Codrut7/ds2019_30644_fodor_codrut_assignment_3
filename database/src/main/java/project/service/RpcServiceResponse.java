package project.service;

import io.grpc.stub.StreamObserver;
import project.Service;
import project.medicationTakenServiceGrpc;

public class RpcServiceResponse extends medicationTakenServiceGrpc.medicationTakenServiceImplBase {


    @Override
    public void taken(Service.Taken request,
                      StreamObserver<Service.Response> responseObserver) {
        // HelloRequest has toString auto-generated.
        System.out.println("Ceva");
        if(request.getTaken()){
            System.out.println("Pacientul a luat medicamentul ...... ");
        }else{
            System.out.println("Pacientul NU a luat medicamentul ......");
        }

        Service.Response response = Service.Response.newBuilder()
                .setResponse(true)
                .build();

        // Use responseObserver to send a single response back
        responseObserver.onNext(response);

        // When you are done, you must call onCompleted.
        responseObserver.onCompleted();
    }
}
