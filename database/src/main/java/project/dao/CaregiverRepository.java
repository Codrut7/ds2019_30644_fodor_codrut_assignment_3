package project.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import project.model.Caregiver;
import project.model.Patient;
import project.model.User;

import java.util.List;

/**
 * JPA generic implementation of the repository queries used on caregiver entity.
 */
@Repository
public interface CaregiverRepository extends CrudRepository<Caregiver, Long> {

    Caregiver findCaregiverByUser(User user);
}
