package project.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import project.model.Doctor;
import project.model.MedicationPlan;

@Repository
public interface MedicationPlanRepository extends JpaRepository<MedicationPlan,Long> {
}
