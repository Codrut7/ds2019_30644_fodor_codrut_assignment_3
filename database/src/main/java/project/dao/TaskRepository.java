package project.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import project.model.Task;

@Repository
public interface TaskRepository extends CrudRepository<Task, Long> {
}
