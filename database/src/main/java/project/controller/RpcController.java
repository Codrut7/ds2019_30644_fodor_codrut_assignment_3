package project.controller;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import project.service.RpcService;
import project.service.RpcServiceResponse;

@Component
public class RpcController implements CommandLineRunner {

    @Autowired
    private RpcService rpcService;

    @Override
    public void run(String... args) throws Exception {
        // Create a new server to listen on port 8080
        Server server = ServerBuilder.forPort(8081)
                .addService(rpcService) 
                .addService(new RpcServiceResponse())
                .build();

        // Start the server
        server.start();

        // Server threads are running in the background.
        System.out.println("Server started");
        // Don't exit the main thread. Wait until server is terminated.
        server.awaitTermination();
    }
}
