package project;

import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.7.0)",
    comments = "Source: service.proto")
public final class medicationTakenServiceGrpc {

  private medicationTakenServiceGrpc() {}

  public static final String SERVICE_NAME = "project.medicationTakenService";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<project.Service.Taken,
      project.Service.Response> METHOD_TAKEN =
      io.grpc.MethodDescriptor.<project.Service.Taken, project.Service.Response>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "project.medicationTakenService", "taken"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              project.Service.Taken.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              project.Service.Response.getDefaultInstance()))
          .setSchemaDescriptor(new medicationTakenServiceMethodDescriptorSupplier("taken"))
          .build();

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static medicationTakenServiceStub newStub(io.grpc.Channel channel) {
    return new medicationTakenServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static medicationTakenServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new medicationTakenServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static medicationTakenServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new medicationTakenServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class medicationTakenServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void taken(project.Service.Taken request,
        io.grpc.stub.StreamObserver<project.Service.Response> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_TAKEN, responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            METHOD_TAKEN,
            asyncUnaryCall(
              new MethodHandlers<
                project.Service.Taken,
                project.Service.Response>(
                  this, METHODID_TAKEN)))
          .build();
    }
  }

  /**
   */
  public static final class medicationTakenServiceStub extends io.grpc.stub.AbstractStub<medicationTakenServiceStub> {
    private medicationTakenServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private medicationTakenServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected medicationTakenServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new medicationTakenServiceStub(channel, callOptions);
    }

    /**
     */
    public void taken(project.Service.Taken request,
        io.grpc.stub.StreamObserver<project.Service.Response> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_TAKEN, getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class medicationTakenServiceBlockingStub extends io.grpc.stub.AbstractStub<medicationTakenServiceBlockingStub> {
    private medicationTakenServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private medicationTakenServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected medicationTakenServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new medicationTakenServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public project.Service.Response taken(project.Service.Taken request) {
      return blockingUnaryCall(
          getChannel(), METHOD_TAKEN, getCallOptions(), request);
    }
  }

  /**
   */
  public static final class medicationTakenServiceFutureStub extends io.grpc.stub.AbstractStub<medicationTakenServiceFutureStub> {
    private medicationTakenServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private medicationTakenServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected medicationTakenServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new medicationTakenServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<project.Service.Response> taken(
        project.Service.Taken request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_TAKEN, getCallOptions()), request);
    }
  }

  private static final int METHODID_TAKEN = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final medicationTakenServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(medicationTakenServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_TAKEN:
          serviceImpl.taken((project.Service.Taken) request,
              (io.grpc.stub.StreamObserver<project.Service.Response>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class medicationTakenServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    medicationTakenServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return project.Service.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("medicationTakenService");
    }
  }

  private static final class medicationTakenServiceFileDescriptorSupplier
      extends medicationTakenServiceBaseDescriptorSupplier {
    medicationTakenServiceFileDescriptorSupplier() {}
  }

  private static final class medicationTakenServiceMethodDescriptorSupplier
      extends medicationTakenServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    medicationTakenServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (medicationTakenServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new medicationTakenServiceFileDescriptorSupplier())
              .addMethod(METHOD_TAKEN)
              .build();
        }
      }
    }
    return result;
  }
}
