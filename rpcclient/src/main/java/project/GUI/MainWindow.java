package project.GUI;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import project.Service;
import project.service.RpcService;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class MainWindow {

    private long milis = 0;
    private Display display;
    private Timer timer1;
    private Timer timer2;
    private Shell shell;


    public MainWindow(){
        // create timer for update medication plan
        final Timer timer3 = new Timer();
        timer3.schedule(new TimerTask() {
            @Override
            public void run(){
                Display.getDefault().asyncExec(new Runnable() {
                    public void run() {
                        milis = 0;
                        timer1.cancel();
                        timer2.cancel();
                        shell.close();
                        display.close();
                        initiateGUI();
                    }
                });
            }
        },3000,1000 * 60 * 60 * 24);
        initiateGUI();
    }

    public void initiateGUI() {
        Service.MedicationPlan medicationPlan = RpcService.getPatientPlan();
        Display display = new Display();
        this.display = display;
        this.shell = new Shell(display);
        shell.open();
        configureShell(shell, medicationPlan);
        shell.setSize(1000,500);
        // run the event loop as long as the window is open
        while (!shell.isDisposed()) {
            // read the next OS event queue and transfer it to a SWT event
            if (!display.readAndDispatch())
            {
                // if there are currently no other OS event to process
                // sleep until the next OS event is available
                display.sleep();
            }
        }

        // disposes all associated windows and their components
        display.dispose();
    }

    private void configureShell(final Shell shell, Service.MedicationPlan medicationPlan) {
        GridLayout layout = new GridLayout(2, false);
        shell.setLayout(layout);
        Label label1 = Util.createLabel(shell,"Name of the patient is :",SWT.NONE);
        Label label2 = Util.createLabel(shell,"Mihaita",SWT.NONE);
        Label label3 = Util.createLabel(shell,"Medication list of the patient is : ", SWT.NONE);
        String medicationList = createMedicationList(medicationPlan);
        Label label4 = Util.createLabel(shell,medicationList, SWT.NONE);
        Label label5 = Util.createLabel(shell, "Starting date of the medicament is : ", SWT.NONE);
        Label label6 = Util.createLabel(shell, new Date(medicationPlan.getStartDate()).toString(), SWT.NONE);
        Label label7 = Util.createLabel(shell, "Ending date of the medicament is is : ", SWT.NONE);
        Label label8 = Util.createLabel(shell, new Date(medicationPlan.getEndDate()).toString(), SWT.NONE);
        Label label9 = Util.createLabel(shell, "Remaining time is : ", SWT.NONE);
        this.timer1 = createTimerLabel(shell, medicationPlan);
        this.timer2 = new Timer();
        // timer pt pastila la server
        timer2.schedule(new TimerTask() {
            @Override
            public void run() {
                RpcService.sendPatientResponse("mihaita", false);
                timer1.cancel();
            }
        }, medicationPlan.getEndDate());
        Button button = Util.createButton(shell, "Medication taken", SWT.RIGHT);
        button.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent selectionEvent) {
                super.widgetSelected(selectionEvent);
                RpcService.sendPatientResponse("mihaita", true);
                timer1.cancel();
                timer2.cancel();
            }
        });
    }

    private Timer createTimerLabel(Shell shell, Service.MedicationPlan medicationPlan) {
        Long remainingTime = getRemainingTime(new Date(medicationPlan.getStartDate()) , new Date(medicationPlan.getEndDate()));
        String remainingTimeString = getStringRemainingTime(remainingTime);
        Label label10 = Util.createLabel(shell, remainingTimeString, SWT.NONE);
        return createTimerForLabel(label10, remainingTime);
    }

    private Timer createTimerForLabel(final Label label10,final Long remainingInitialTime) {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                milis += 1000;
                final String time = getStringRemainingTime(remainingInitialTime - milis);
                Display.getDefault().asyncExec(new Runnable() {
                    public void run() {
                        label10.setText(time);
                        label10.getParent().layout();
                    }
                });
            }
        },1000,1000);

        return timer;
    }

    private String getStringRemainingTime(Long remainingTime) {
        return String.format("%02d min, %02d sec",
                TimeUnit.MILLISECONDS.toMinutes(remainingTime),
                TimeUnit.MILLISECONDS.toSeconds(remainingTime) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(remainingTime))
        );
    }

    private long getRemainingTime(Date startDate, Date endDate) {
        long diffInMillies = Math.abs(endDate.getTime() - startDate.getTime());
        long diff = TimeUnit.HOURS.convert(diffInMillies, TimeUnit.MILLISECONDS);
        return diffInMillies;
    }

    private String createMedicationList(Service.MedicationPlan medicationPlan) {
        StringBuffer buffer = new StringBuffer();
        for(int i =0;i<medicationPlan.getMedicationsList().size();i++){
            buffer.append(medicationPlan.getMedications(i).getName() + "  ");
        }
        return buffer.toString();
    }

    public Display getDisplay() {
        return display;
    }

    public void setDisplay(Display display) {
        this.display = display;
    }
}
