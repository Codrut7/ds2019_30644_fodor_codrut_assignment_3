package project.GUI;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class Util {

    public static Label createLabel(Shell shell, String text, int style){
        Label label = new Label(shell,style);
        label.setText(text);
        FontData[] fD = label.getFont().getFontData();
        fD[0].setHeight(24);
        label.setFont(new Font(shell.getDisplay(),fD));
        return label;
    }

    public static Button createButton(Shell shell, String text, int style){
        Button button = new Button(shell, style);
        button.setText(text);

        return button;
    }
}
