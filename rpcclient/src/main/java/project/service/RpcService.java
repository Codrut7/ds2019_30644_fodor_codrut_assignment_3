package project.service;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import project.GUI.MainWindow;
import project.Service;
import project.medicationTakenServiceGrpc;
import project.sendMedicationPlanServiceGrpc;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RpcService extends medicationTakenServiceGrpc.medicationTakenServiceImplBase{


    public static void sendPatientResponse(String name, boolean medicineTaken){
        // Let's use plaintext communication because we don't have certs
        final ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:8081")
                .usePlaintext(true)
                .build();

        // It is up to the client to determine whether to block the call
        // Here we create a blocking stub, but an async stub,
        // or an async stub with Future are always possible.
        medicationTakenServiceGrpc.medicationTakenServiceBlockingStub stub = medicationTakenServiceGrpc.newBlockingStub(channel);
        Service.Taken request = Service.Taken.newBuilder()
                .setName(name)
                .setTaken(medicineTaken)
                .build();
        // Finally, make the call using the stub
        Service.Response response =
                stub.taken(request);

        System.out.println("Raspuns primit de la host : " + response.getResponse());
        // A Channel should be shutdown before stopping the process.
        channel.shutdownNow();
    }

    public static Service.MedicationPlan getPatientPlan(){
        // Channel is the abstraction to connect to a service endpoint
        // Let's use plaintext communication because we don't have certs
        final ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:8081")
                .usePlaintext(true)
                .build();

        // It is up to the client to determine whether to block the call
        // Here we create a blocking stub, but an async stub,
        // or an async stub with Future are always possible.
        sendMedicationPlanServiceGrpc.sendMedicationPlanServiceBlockingStub stub = sendMedicationPlanServiceGrpc.newBlockingStub(channel);
        Service.userName request =
                Service.userName.newBuilder()
                        .setName("mihaita")
                        .build();

        // Finally, make the call using the stub
        Service.MedicationPlan response =
                stub.send(request);
        // A Channel should be shutdown before stopping the process.
        channel.shutdownNow();

        return response;
    }
}
